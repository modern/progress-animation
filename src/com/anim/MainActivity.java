package com.anim;

import android.os.Bundle;
import android.widget.ImageView;
import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;

public class MainActivity extends Activity {
	
	AnimationDrawable ProgressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        ImageView imageProgress = (ImageView)findViewById(R.id.imageProgress);
        
        ProgressBar = (AnimationDrawable)imageProgress.getDrawable();
        imageProgress.post(new Runner());
        }
     
        class Runner implements Runnable {
            public void run() {
            	ProgressBar.start();
            }
        }
 }

